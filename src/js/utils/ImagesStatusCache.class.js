/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
 
 
 /**
 * A cache for images
 */
var ImagesStatusCache = { 'images': {} };

/**
 * Return an image with his status of the cache
 * @param {string} An image path
 * @return {ImageStatus} The image that match the path
 */
ImagesStatusCache.getImageStatus = function(img_path)
{
	if(typeof(ImagesStatusCache.images[img_path]) == 'undefined' ||
	   !(ImagesStatusCache.images[img_path] instanceof ImageStatus))
	{
		ImagesStatusCache.images[img_path] = new ImageStatus(img_path);
	}
	return ImagesStatusCache.images[img_path];
}

/**
 * Return an image of the cache
 * @param {string} An image path
 * @return {Image} The image that match the path
 */
ImagesStatusCache.getImage = function(img_path)
{
	return ImagesStatusCache.getImageStatus(img_path).image;
}
