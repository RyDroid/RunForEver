# Run for Ever

## Contribute

[You are welcome](https://gitlab.com/RyDroid/RunForEver).

### Files

#### Text file "settings"

All text files have to be in [UTF-8](https://en.wikipedia.org/wiki/UTF-8) (without BOM) encoding (generally UTF-8 on GNU/Linux and ISO 8859-15 on Windows) and LF character newline (sometime called UNIX newline/endline) (Windows generally used CRLF).

#### Source code

##### Coding style

We use english, so please use it everywhere in the project (messages, function names, doc, etc).

Names of variables, functions/methods, classes and everything else have to be clear, even if the name is a little longer.
You also do not have to forget to create documentation.

Classes should be written in upper camel case.
Variables, functions and methods should be written in lower camel case.
Constants should be written in uppercase characters separated by underscores.

Following [the PEP (Python Enhancement Proposal) 20](https://www.python.org/dev/peps/pep-0020/) is a good thing, even if the project does not use the Python language.

utils folder contains files that can be useful in an other not similar project.

#### Formatted text files

If you need to make a structured document, you should consider Markdown and of course HTML.
For example, this document uses the Markdown syntax.

#### Images, sounds and videos

Images, sounds and videos can be a lot of formats.
Each image, sound and video have to be in a at least one free/open format (known specifications, without patents and openable in 100% free/libre environment) and without [DRM](https://www.defectivebydesign.org/).

## License and authors

See [LICENSE.md](LICENSE.md) and logs of git for the full list of contributors of the project.
